
OVERVIEW
========

This module provides bounce-urls, i.e., urls that redirect user to
a target. The redirects are logged, which enables click
tracking. See '<your-site>/admin/help/bouncer' for usage.


SUPPORTED DATABASE BACKENDS
===========================

This module is developed for:
- MySQL 5.0.XX
- PostgreSQL 8.0.XX
Older versions might also work.


ACKNOWLEDGEMENTS
================
The development of this module was funded by Kustannusosakeyhtiö Uusi
Suomi, which runs news site www.uusisuomi.fi (in Finnish).
